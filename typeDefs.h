/*
 * Contain general definitions
 */

#ifndef TYPE_DEFS_H
#define TYPE_DEFS_H

/* Include ********************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

/* Defines ********************************************************************/

typedef int64_t     int64;
typedef uint64_t    uint64;
typedef int32_t     int32;
typedef uint32_t    uint32;
typedef int16_t     int16;
typedef uint16_t    uint16;
typedef int8_t      int8;
typedef uint8_t     uint8;
typedef unsigned int uint;

typedef const char  cchar;


typedef size_t size;


typedef enum
{
    FALSE = 0,
    TRUE,
} bool;

/* Macros *********************************************************************/



#endif /* TYPE_DEFS_H */
